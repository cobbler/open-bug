<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		#header {height: 30px;background: rgb(0, 51, 102);color: white;padding: 10px 15px 0 10px;}
		#header .logo {font-size: 22px;font-weight: bold;}
		#header .operate {float: right;display: inline-block;margin-right: 20px;}
		#header .dropdown-toggle {color: white!important;text-decoration: none;}
		#header .dropdown-toggle:hover {text-decoration: underline;}
		#mainContent {margin: 5px 10px 15px 10px;}
		#nav {margin-bottom: -19px;}
		#footer {clear: both;border-top: 1px solid #cccccc;text-align: center;padding: 5px;}
		#footer span {float: right;margin-right: 20px;}
	</style>
</head>

<body>
	<div id="header">
		<span class="logo">PPM Bug Manage</span>
		<span class="operate">
			<div class="dropdown">
				<a data-toggle="dropdown" class="dropdown-toggle" href="#"><sec:authentication property="name"/><i class="icon-chevron-down icon-white" style="vertical-align: middle;margin-left: 3px;margin-bottom: 3px;"></i></a>
				
				<ul class="dropdown-menu" style="max-width: 60px;min-width: 60px;">
					<li><a href="#" style="padding: 3px 15px;" onclick="showLock();return false;">锁定</a></li>
					<li><a href="<c:url value="/user/logout.do" />" style="padding: 3px 15px;">退出</a></li>
				</ul>
			</div>
		</span>
	</div>
	
	<div id="mainContent">
		<div id="nav">
			<ul class="nav nav-tabs">
				<li url="<c:url value="/work/openWork.do" />" class="active"><a href="#">工作面板</a></li>
				<li url="<c:url value="/pages/project/project.jsp" />"><a href="#">项目状态</a></li>
				<li url="<c:url value="/pages/bug/bug.jsp" />"><a href="#">缺陷列表</a></li>
				<li url="<c:url value="/pages/account/account.jsp" />"><a href="#">用户列表</a></li>
				<li url="<c:url value="/pages/system/system.jsp" />"><a href="#">系统管理</a></li>
			</ul>
		</div>
		
		<iframe id="mainFrame" name="mainFrame" src="<c:url value="/work/openWork.do" />" width="100%" 
			onload="autoHeight();" frameborder="0" style="border: 1px solid #ccc;border-top: 0;border-radius: 0 0 10px 10px;"></iframe>
	</div>
	
	<div id="loader">
		<div>
			<img src="<c:url value="/img/loader.gif" />" /><br />
			<span id="loaderTip"></span>
		</div>
	</div>
	
	<div class="hide">
		<span id="loaderTip1">正在加载...</span>
	</div>
	
	<div id="lockModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<h3 id="myModalLabel">系统锁定</h3>
		</div>
		
		<div class="modal-body">
			<span style="margin: 0 20px;">密码：</span><input id="lockPwd" type="password" style="width: 380px;height: 28px;" />
			<span id="lockTip" class="outline hide">密码错误</span>
		</div>
		
		<div class="modal-footer">
			<button onclick="unLock();" class="btn btn-primary">确认</button>
		</div>
	</div>
	
	<script>
		var basePath = "/open-bug/";
		
		$("#nav li").click(function() {
			$("#nav li").removeClass("active");
			$(this).addClass("active");
			
			$("#mainFrame").attr("src", $(this).attr("url"));
		});
		
		$("#lockModal").modal({backdrop: "static", show: false});
		
		function showLock() {
			$("#lockPwd").val("");
			$("#lockTip").hide();
			$("#lockModal").modal("show");
		}
		
		function unLock() {
			_remoteCall("user/unlock.do", {password: $("#lockPwd").val()}, function(data) {
				if(data == "Y") {
					$("#lockTip").hide();
					$("#lockModal").modal("hide");
				} else {
					$("#lockPwd").val("").focus();
					$("#lockTip").show();
				}
			});
		}
		
		/**
		 * iframe 自适应高度
		*/
		function autoHeight() {
			var ifm = document.getElementById("mainFrame");
			var h = $("div.wrapper", ifm.contentDocument.body).outerHeight() + 20;
			var minH = document.body.clientHeight - 100;
			
			ifm.height = h < minH ? minH : h; 
		}
	</script>
</body>
</html>
