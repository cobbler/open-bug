<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cloud" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<script type="text/javascript" src="<c:url value="/scripts/highcharts.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/stat.chart.js" />"></script>
	
	<style>
		body {overflow: hidden;}
		td.col {vertical-align: top;}
		table {font-size: 13px;}
		.box {margin: 8px;border: 1px solid #ccc;border-radius: 5px;margin-bottom: 20px;}
		.box .title {padding: 7px;font-weight: bold;border-bottom: 1px solid #ccc;}
		.box .content {padding: 7px;overflow-x: auto;}
		.record-1 {display: inline-block;min-width: 60px;max-width: 60px;}
		.record-2 {display: inline-block;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
		.record-3 {}
		.input-text {margin-bottom: 0!important;}
	</style>
</head>

<body>
	<div class="wrapper">
	
	<table id="containerTab" style="table-layout: fixed;">
	<tr>
	<td class="col">
		<div style="padding: 10px;">
			<a href="<c:url value="/bug/openOperate.do?op=create&fromStatus=0&workspace=Y" />" class="btn">新建缺陷</a>
		</div>
		
		<div class="box">
			<div class="title">我的项目</div>
			<div class="content">
				<table id="projectTab" class="list-table" style="width: 540px;">
					<tr>
						<th width="40px">序号</th>
						<th width="350px">名称</th>
						<th width="150px">创建时间</th>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="box">
			<div class="title">
				我的缺陷
				<span class="view-bar">
					<div class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><span id="viewName">默认视图</span><i class="icon-th-list"></i></a>
						
						<ul class="dropdown-menu">
							<li id="defaultView"><a href="#" onclick="selectView();return false;">默认视图</a></li>
						</ul>
					</div>
				</span>
			</div>
			
			<div class="content">
				<table id="bugTab" class="list-table" style="width: 740px;">
					<tr>
						<th width="40px"></th>
						<th width="300px"><input id="name" type="text" class="input-text" style="width: 295px;" onkeydown="textSearch();" /></th>
						<th width="150px"><input id="project" type="text" class="input-text" style="width: 145px;" ondblclick="showProjects($(this));" onkeydown="return false;" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon" style="margin-left: -19px;margin-top: 5px;"></i></th>
						<th width="100px"><input id="status" type="text" class="input-text" style="width: 95px;" ondblclick="showStatuses($(this));" onkeydown="return false;" /><i onclick="$(this).prev().dblclick();" class="icon-search input-icon" style="margin-left: -19px;margin-top: 5px;"></i></th>
						<th width="150px"></th>
					</tr>
					<tr id="head-tr">
						<th>序号</th>
						<th><span class="head-sort" field="name">名称</span></th>
						<th><span class="head-sort" field="projectId">所属项目</span></th>
						<th><span class="head-sort" field="status">状态</span></th>
						<th><span class="head-sort" field="modifyTime">最近更新时间<i class="icon-arrow-down"></i></span></th>
					</tr>
				</table>
				
				<div class="pagination"><ul></ul></div>
			</div>
		</div>
	</td>
	
	<td class="col">
		<div class="box">
			<div class="title">缺陷状态统计图</div>
			<div id="statusChart" class="content"></div>
		</div>
		
		<div class="box">
			<div class="title">项目缺陷统计图</div>
			<div id="bugProjectChart" class="content"></div>
		</div>
	</td>
	
	<td class="col">
		<div class="box">
			<div class="title">系统动态</div>
			<div id="recordContent" class="content">
				<div style="margin-left: 20px;">无</div>
			</div>
		</div>
	</td>
	</tr>
	</table>
	
	</div>
	
	<jsp:include page="/pages/common/projectSelectMul.jsp"></jsp:include>
	<jsp:include page="/pages/common/statusSelectMul.jsp"></jsp:include>
	
	<script>
		$(function() {
			$("#containerTab").width(document.body.clientWidth - 45);
			$("td.col").css({"max-width": document.body.clientWidth / 3 - 15, "min-width": document.body.clientWidth / 3 - 15});
		});
		
		// init my projects
		var info = ${projects}, html = "";
		
		for(var i in info) {
			html += "<tr id='" + info[i].id + "'>";
			html += "<td class='sn'>" + (parseInt(i) + 1) + "</td>";
			html += "<td><div class='auto-link'><a href='#' onclick='openProject($(this));return false;'>" + info[i].name + "</a></div></td>";
			html += "<td><div>" + getTimeStr(info[i].createTime) + "</div></td>";
			html += "</tr>";
		}
		
		$("#projectTab").append(html);
		
		// init my bugs view
		_remoteCall("bug/getViews.do", null, function(data) {
			var info = eval(data), html = "";
			
			for(var i in info) {
				html += "<li><a href='#' onclick='selectView(\"" + info[i].id + "\");return false;'><span onmouseover='this.title=this.innerText' style='width: 115px;display: inline-block;' class='text-omit'>" + info[i].name + "</span></a></li>";
				if(info[i].isDefault == "Y")  $("#viewName").text(info[i].name);
			}
			
			$("#defaultView").after(html);
		});
		
		// init my bugs
		var sortInfo = [];
		search();
		initSort();
		
		function search(toPage) {
			_remoteCall("bug/getBugs.do", {workspace: "Y", page: toPage ? toPage : 1, sort: sortInfo.join(","), name: $("#name").val(), projectIds: $("#project").attr("val") ? $("#project").attr("val") : "", status: $("#status").attr("val") ? $("#status").attr("val") : ""}, function(data) {
				var dataInfo = eval("(" + data + ")"), page = dataInfo.page, info = dataInfo.bugs, html = "";
				
				// init bugs table
				for(var i in info) {
					html += "<tr id='" + info[i].id + "'>";
					html += "<td class='sn'>" + getPageSn(page, i) + "</td>";
					html += "<td><div class='auto-link'><a href='#' onclick='openBug($(this));return false;'>" + info[i].name + "</a></div></td>";
					html += "<td><div>" + info[i].projectName + "</div></td>";
					html += "<td><div>" + info[i].statusName + "</div></td>";
					html += "<td><div>" + getTimeStr(info[i].modifyTime) + "</div></td>";
					html += "</tr>";
				}
				
				// remove old trs first
				$("#bugTab tr:gt(1)").remove();
				$("#bugTab").append(html);
				
				// init page bar
				initPage(page);
				
				parent.autoHeight();
			});
		}
		
		function textSearch() {
			if(event.keyCode != 13)  return;
			search();
		}
		
		function initSort() {
			$("span.head-sort").click(function() {
				sortInfo = [];
				var $i = $("#head-tr i"), $spanI = $("i", $(this));
				sortInfo.push($(this).attr("field"));
				
				if($spanI.size() == 0) {
					$(this).append($i.removeClass("icon-arrow-down").addClass("icon-arrow-up"));
					sortInfo.push("asc");
				} else {
					if($spanI.hasClass("icon-arrow-up")) {
						$spanI.removeClass("icon-arrow-up").addClass("icon-arrow-down");
						sortInfo.push("desc");
					} else {
						$spanI.removeClass("icon-arrow-down").addClass("icon-arrow-up");
						sortInfo.push("asc");
					}
				}
				
				search();
			});
		}
		
		function selectView(viewId) {
			location.href = parent.basePath + "work/selectView.do?viewId=" + (viewId ? viewId : "");
		}
		
		// init bug status chart
		var statusStat = ${statusStat};
        initPieChart("statusChart", "缺陷状态统计图", statusStat);
        
        // init project bug chart
        var bugProjectChart = ${bugPjtStat};
        initPieChart("bugProjectChart", "项目缺陷统计图", bugProjectChart);
        
        // init system activities
        info = ${records};  html = combineRecordHtml(info);
        
        if("${hasMore}" == "Y") {
        	html += "<div id='hasMore' class='more' onclick='showMoreRecord();'>显示更多 <i class='icon-chevron-down' style='vertical-align: middle;margin-left: 3px;margin-bottom: 6px;'></i></div>";
        }
        
        $("#recordContent").html(html);
		
        /**
         * ==========================  functions  ==========================
        */
        function openProject($a) {
			var id = $a.closest("tr").attr("id");
			location.href = parent.basePath + "project/openProject.do?projectId=" + id + "&workspace=Y";
		}
        
        function openBug($a, noTr) {
			var id = !noTr ? $a.closest("tr").attr("id") : $a.attr("id");
			location.href = parent.basePath + "bug/openBug.do?bugId=" + id + "&workspace=Y";
		}
        
        var page = 1;
        
        function combineRecordHtml(info) {
        	var html = "";
        	var fixW = document.body.clientWidth / 3 - 15 - 60 - 155;
        	
        	for(var i in info) {
            	html += "<div class='record_box'>";
            	html += "<div>";
            	html += "<span class='record-1'>" + info[i].creator + "</span>";
            	html += "<span class='record-2' style='min-width: " + fixW + "px;max-width: " + fixW + "px;margin-right: 5px;'>" + info[i].opName + " <a id='" + info[i].bugId + "' onclick='openBug($(this), true);return false;' href='#' onmouseover='this.title=this.innerText'>" + info[i].bugName + "</a></span>";
    			html += "<span class='record-3'>" + getTimeStr(info[i].createTime, true) + "</span>";
    			html += "</div>";
    			html += "<div class='record_note'>" + info[i].note + "</div>";
    			html += "</div>";
            }
        	
        	return html;
        }
        
        function showMoreRecord() {
        	_remoteCall("/work/moreRecord.do", {page: ++page}, function(data) {
        		
        		var d = eval("(" + data + ")"), hasMore = d.hasMore, info = d.records;
        		
        		$("#hasMore").before(combineRecordHtml(info));
        		if(hasMore == "N")  $("#hasMore").remove();
        		
        		parent.autoHeight();
        	});	
        }
	</script>
</body>
</html>
