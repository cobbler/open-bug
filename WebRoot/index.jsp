<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<style>
		body {text-align: center;}
		table {margin: 15% auto;}
		table tr {height: 50px;}
	</style>
</head>

<body>
	<form action="j_spring_security_check" method="POST">
	<table>
		<tr>
			<td colspan="2" align="center" style="font-weight: bold;">PPM 缺陷管理</td>
		</tr>
		<tr>
			<td align="right">用户名：</td>
			<td><input type="text" name="j_username" class="input-text" placeholder="Username" value="ross" /></td>
		</tr>
		<tr>
			<td align="right">密码：</td>
			<td><input type="password" name="j_password" class="input-text" placeholder="Password" value="1" /></td>
		</tr>
		<tr style="height: 20px;">
			<td colspan="2" align="center" style="color: red;font-size: 12px;">
				<c:if test="${param.error == null}">推荐使用Chrome浏览器，暂不支持IE系列</c:if>
				<c:if test="${param.error != null}">用户名或密码输入错误！</c:if>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input class="btn" type="submit" />
				<input class="btn" type="reset" />
			</td>
		</tr>
	</table>
	</form>
</body>
</html>
