package com.cloud.security.web;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloud.platform.Constants;
import com.cloud.security.model.User;
import com.cloud.security.service.UserService;

@Controller
@RequestMapping("user")
public class UserBean {
	
	@Autowired
	private UserService userService;
	
	/**
	 * check if is admin
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/isAdmin.do")
	public String isAdmin() {
		
		return Constants.getLoginUserId().equals(
				"000000000000000000000000000000000000") ? Constants.VALID_YES : Constants.VALID_NO;
	}
	
	/**
	 * get user's password
	 * 
	 * @param userId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getPassword.do")
	public String getPassword(@RequestParam("userId") String userId) {
		
		User u = userService.getUserById(userId);
		
		return u.getPassword();
	}
	
	/**
	 * remove user
	 * 
	 * @param userId
	 */
	@ResponseBody
	@RequestMapping("/removeUser.do")
	public void removeUser(@RequestParam("userId") String userId) {
		
		userService.removeUser(userId);
	}
	
	/**
	 * chagne password
	 * 
	 * @param password
	 */
	@ResponseBody
	@RequestMapping("/changePwd.do")
	public void changePwd(@RequestParam("password") String password) {
		
		userService.changePwd(Constants.getLoginUserId(), password);
	}
	
	/**
	 * unlock system
	 * 
	 * @param password
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/unlock.do")
	public String unlock(@RequestParam("password") String password) {
		
		User u = userService.getUserById(Constants.getLoginUserId());
		
		return (u != null && u.getPassword().equals(password)) ? Constants.VALID_YES
				: Constants.VALID_NO;
	}
	
	/**
	 * account logout
	 * 
	 * @param session
	 * @param response
	 */
	@RequestMapping("/logout.do")
	public void logout(HttpSession session, HttpServletResponse response) {

		try {
			if(session != null) {
				session.invalidate();
			}
			
			response.sendRedirect(Constants.BASEPATH + "index.jsp");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get type users list info
	 * 
	 * @param type
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getUsers.do")
	public String getUsers(@RequestParam("type") int type) {

		// search users
		List users = userService.searchUsers(type);
		
		// convert to json format
		JSONArray usersArr = JSONArray.fromObject(users);
		
		return usersArr.toString();
	}

	/**
	 * save user
	 * 
	 * @param type
	 * @param username
	 * @param password
	 * @param email
	 * @param address
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/saveUser.do")
	public String saveUser(User user) {
		
		userService.saveUser(user);
		
		return "";
	}
}
